# Assignment 1 - RPGHeroes

RPGHeroes is a Java-program which simulates an RPG game with weapons, heroes and armor through classes and properties.

## Installation
- Install Java JDK 17
- Clone the repository

## Usage
- Open the repository in IntelliJ
- Create a run-configuration with Gradle and run tests

OR
- Right-click the "test"-folder in the project structure and click on 'Run 'tests in assignment...'

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)