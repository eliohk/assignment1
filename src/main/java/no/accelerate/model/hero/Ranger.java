package no.accelerate.model.hero;

import no.accelerate.model.equipment.Armor.ArmorType;
import no.accelerate.model.equipment.Item;
import no.accelerate.model.equipment.Weapon;
import no.accelerate.model.equipment.Weapon.WeaponType;

/**
 * <h1>Ranger</h1>
 * Ranger is a subclass of Hero, and is a representation of the class 'Ranger' from typical RPG's.
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class Ranger extends Hero {
    private HeroAttribute statIncr = new HeroAttribute(1, 5, 1);

    /**
     * A ranger-object is created with only a name, however the super-constructor initializes
     * the remaining necessary fields which heroes need.
     *
     * @param name Display name of the Hero
     */
    public Ranger(String name) {
        super(name, "Ranger", new int[]{1, 7, 1},
                new WeaponType[]{WeaponType.Bow},
                new ArmorType[]{ArmorType.Leather, ArmorType.Mail});
    }

    public void levelUp() {
        super.incrementLevel();
        super.getLevelAttributes().add(statIncr);
    }

    @Override
    public double damage() {
        HeroAttribute total = totalAttributes();
        double modifier = total.getDex();
        if (getInventory().get(Item.Slot.Weapon) != null) {
            double weaponDamage = (((Weapon)getInventory().get(Item.Slot.Weapon)).getWeaponDamage());
            return weaponDamage * (1.0 + modifier/100.0);
        } else {
            return (1.0 + modifier / 100.0);
        }
    }
}
