package no.accelerate.model.hero;

import no.accelerate.model.exception.InvalidArmorException;
import no.accelerate.model.exception.InvalidWeaponException;
import no.accelerate.model.equipment.Armor;
import no.accelerate.model.equipment.Armor.ArmorType;
import no.accelerate.model.equipment.Item;
import no.accelerate.model.equipment.Item.Slot;
import no.accelerate.model.equipment.Weapon;
import no.accelerate.model.equipment.Weapon.WeaponType;

import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Hero</h1>
 * An abstract class which contains shared fields for 'Hero'-subclasses as well as public methods with basic behaviour
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public abstract class Hero {
    private String name;
    private String heroClass;
    private int level;
    private HeroAttribute levelAttributes;
    private Map<Slot, Item> equipment;
    private WeaponType[] validWeaponTypes;
    private ArmorType[] validArmorTypes;

    /**
     * The constructor which is responsible for initializing all fields for a Hero-subclass.
     * @param name Display name of the hero
     * @param heroClass Generalized class of the hero
     * @param startingStats The stats (strength, dexterity, intelligence) which the hero starts with from level 1
     * @param validWeaponTypes An array containing valid weapon types
     * @param validArmorTypes An array containing valid armor types
    */
    Hero (String name, String heroClass, int[] startingStats, WeaponType[] validWeaponTypes, ArmorType[] validArmorTypes) {
        this.name = name;
        this.validArmorTypes = validArmorTypes;
        this.validWeaponTypes = validWeaponTypes;
        levelAttributes = new HeroAttribute(startingStats[0], startingStats[1], startingStats[2]);
        this.heroClass = heroClass;
        level = 1;

        equipment = new HashMap<>();
        // Initialize inventory
        equipment.put(Slot.Head, null);
        equipment.put(Slot.Body, null);
        equipment.put(Slot.Legs, null);
        equipment.put(Slot.Weapon, null);

    }

    // Abstract methods
    /**
     * Calculates the damage on the fly based on equipment and stats.
     *
     * @return double The damage the Hero should inflict when attacking
     */
    public abstract double damage(); // Calculates damage on the fly

    /**
     * Increases the hero's level by incrementing the 'level'-variable and
     * increasing the hero's stats by adding an instance of HeroAttribute with a preset incrementation of each attribute
     * to the hero's own levelAttributes
     */
    public abstract void levelUp();
    // ----------------------

    /**
     * This method is used as a helper-function in each subclass to increment the 'level'-variable
     */
    protected void incrementLevel() {
        level++;
    }

    /**
     * This method is used to equip a subclass of Item, a piece of Armor.
     * @param armor The armor which is to be equipped
     * @throws InvalidArmorException Thrown if level is too high, or if the hero cannot wield this particular armor type.
     */
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (!(level >= armor.getLevel())) {
            throw new InvalidArmorException("[" + this.name + "] " + armor.getName() + " requires a level of " + armor.getLevel() + " to be equipped.");
        }

        for (ArmorType validArmor : validArmorTypes) {
            if (validArmor.equals(armor.getType())) {
                equipment.put(armor.getSlot(), armor);
                System.out.println("[" + this.name + "] " + armor.getName() + " successfully equipped!");
                return;
            }
        }

        throw new InvalidArmorException("[" + this.name + "] " + armor.getName() + " is not an armor which the class " + heroClass + " can use!");
    }
    /**
     * This method is used to equip a subclass of Item, a Weapon.
     * @param weapon The weapon which is to be equipped
     * @throws InvalidWeaponException Thrown if level is too high, or if the hero cannot wield this particular weapon type.
     */
    public void equipWeapon (Weapon weapon) throws InvalidWeaponException {
        if (!(level >= weapon.getLevel())) {
            throw new InvalidWeaponException("[" + this.name + "] " + weapon.getName() + " requires a level of " + weapon.getLevel() + " to be equipped.");
        }

        for (WeaponType validWeapon : validWeaponTypes) {
            if (validWeapon.equals(weapon.getType())) {
                equipment.put(weapon.getSlot(), weapon);
                System.out.println("[" + this.name + "] " + weapon.getName() + " successfully equipped!");
                return;
            }
        }

        throw new InvalidWeaponException("[" + this.name + "] " + weapon.getName() + " is not a weapon which the class " + heroClass + " can use!");
    }

    /**
     * This method returns the total attributes from the hero's stats and armor stats
     * @return HeroAttribute This returns the sum of HeroAttributes from levels and each piece of Armor
     */
    public HeroAttribute totalAttributes () {
        int[] armorAttributes = {0,0,0};
        for (Item armor: getInventory().values()) {
            if (armor != null && armor.getSlot() != Slot.Weapon) {
                armorAttributes[0] += ((Armor)armor).getArmorAttribute().getStr();
                armorAttributes[1] += ((Armor)armor).getArmorAttribute().getDex();
                armorAttributes[2] += ((Armor)armor).getArmorAttribute().getInt();
            }
        }

        HeroAttribute tempAttributes = new HeroAttribute(armorAttributes[0], armorAttributes[1], armorAttributes[2]);

        return tempAttributes.add(getLevelAttributes());
    }

    /**
     * @return String Returns a neat string which can be printed
     */
    public String display() {
        return "--------------------------\n" +
                "Name: " + getName() +
                "\nClass: " + getHeroClass() +
                "\nlevel: " + getLevel() +
                "\n\n > TotalAttributes \n" + totalAttributes() +
                "\n\nDamage: " + damage() + "\n";
    }

    // Getters
    public int getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public HeroAttribute getLevelAttributes() {
        return levelAttributes;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public Map<Slot, Item> getInventory() {
        return equipment;
    }
}
