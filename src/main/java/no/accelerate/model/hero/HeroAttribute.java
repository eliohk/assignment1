package no.accelerate.model.hero;

import java.util.Objects;

/**
 * <h1>HeroAttribute</h1>
 * An encapsulated class which contains three attributes (strength, dexterity, intelligence)
 * and a function which adds the three attributes of another heroAttributes instance to this.
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class HeroAttribute {
    private int str;
    private int dex;
    private int intel;

    /**
     * The constructor initializes the three attributes
     *
     * @param str determines the physical strength of the character
     * @param dex determines the characters ability to attack with speed and nimbleness
     * @param intel determines the characters affinity with magic
     */
    public HeroAttribute(int str, int dex, int intel) {
        this.str = str;
        this.dex = dex;
        this.intel = intel;
    }

    /**
     * @param attributes the instance to perform addition with
     * @return HeroAttribute the instance which calls the add()-method
     */
    public HeroAttribute add(HeroAttribute attributes) {
        str += attributes.getStr();
        dex += attributes.getDex();
        intel += attributes.getInt();
        return this;
    }

    /**
     * Helper-function to be used in display() of the Hero-class.
     *
     * @return String which has prettified the three attributes
     */
    public String toString() {
        return " - Strength: " + str + "\n" +
                " - Dexterity: " + dex + "\n" +
                " - Intelligence: " + intel;
    }

    // Getters and automatically generated methods

    public int getStr() {
        return str;
    }

    public int getDex() {
        return dex;
    }

    public int getInt() {
        return intel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeroAttribute that = (HeroAttribute) o;
        return str == that.str && dex == that.dex && intel == that.intel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(str, dex, intel);
    }
}
