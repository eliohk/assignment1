package no.accelerate.model.hero;

import no.accelerate.model.equipment.Armor.ArmorType;
import no.accelerate.model.equipment.Item;
import no.accelerate.model.equipment.Weapon;
import no.accelerate.model.equipment.Weapon.WeaponType;

/**
 * <h1>Warrior</h1>
 * Warrior is a subclass of Hero, and is a representation of the class 'Warrior' from typical RPG's.
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class Warrior extends Hero{
    private HeroAttribute statIncr = new HeroAttribute(3, 2, 1);

    /**
     * A warrior-object is created with only a name, however the super-constructor initializes
     * the remaining necessary fields which heroes need.
     *
     * @param name Display name of the Hero
     */
    public Warrior(String name) {
        super(name, "Warrior", new int[]{5, 2, 1},
                new WeaponType[]{WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword},
                new ArmorType[]{ArmorType.Mail, ArmorType.Plate});
    }

    public void levelUp() {
        super.incrementLevel();
        super.getLevelAttributes().add(statIncr);
    }

    @Override
    public double damage() {
        HeroAttribute total = totalAttributes();
        double modifier = total.getStr();
        if (getInventory().get(Item.Slot.Weapon) != null) {
            double weaponDamage = (((Weapon)getInventory().get(Item.Slot.Weapon)).getWeaponDamage());
            return weaponDamage * (1.0 + modifier/100.0);
        } else {
            return (1.0 + modifier / 100.0);
        }
    }
}
