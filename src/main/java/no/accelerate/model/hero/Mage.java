package no.accelerate.model.hero;

import no.accelerate.model.equipment.Armor.ArmorType;
import no.accelerate.model.equipment.Item;
import no.accelerate.model.equipment.Weapon;
import no.accelerate.model.equipment.Weapon.WeaponType;

/**
 * <h1>Mage</h1>
 * Mage is a subclass of Hero, and is a representation of the class 'Mage' from typical RPG's.
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class Mage extends Hero{
    private HeroAttribute statIncr = new HeroAttribute(1, 1, 5);

    /**
     * A mage-object is created with only a name, however the super-constructor initializes
     * the remaining necessary fields which heroes need.
     *
     * @param name Display name of the Hero
     */
    public Mage(String name) {
        super(name, "Mage", new int[]{1, 1, 8},
                new WeaponType[]{WeaponType.Staff, WeaponType.Wand},
                new ArmorType[]{ArmorType.Cloth});
    }

    @Override
    public void levelUp() {
        super.incrementLevel();
        super.getLevelAttributes().add(statIncr);
    }

    @Override
    public double damage() {
        HeroAttribute total = totalAttributes();
        double modifier = total.getInt();

        if (getInventory().get(Item.Slot.Weapon) != null) {
            double weaponDamage = (((Weapon)getInventory().get(Item.Slot.Weapon)).getWeaponDamage());
            return weaponDamage * (1.0 + modifier/100.0);
        } else {
            return (1.0 + modifier / 100.0);
        }
    }
}

