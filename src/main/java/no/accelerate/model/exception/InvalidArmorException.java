package no.accelerate.model.exception;

/**
 * <h1>InvalidArmorException</h1>
 * A custom exception which is thrown when a Hero cannot wield a piece of armor
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class InvalidArmorException extends Exception{
    /**
     * @param error The error message
     */
    public InvalidArmorException(String error) {
        super(error);
    }

}
