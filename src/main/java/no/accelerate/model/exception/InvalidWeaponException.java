package no.accelerate.model.exception;

/**
 * <h1>InvalidWeaponException</h1>
 * A custom exception which is thrown when a Hero cannot wield a weapon
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class InvalidWeaponException extends Exception{
    /**
     * @param error The error message
     */
    public InvalidWeaponException(String error) {
        super(error);
    }
}
