package no.accelerate.model.equipment;

import no.accelerate.model.hero.HeroAttribute;

/**
 * <h1>Armor</h1>
 * Armor is a subclass of the abstract class 'Item', which represents any equippable item
 * which does not have the Slot "Weapon".
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class Armor extends Item {
    /**
     * An enum to categorize the piece of armor into specific
     * types which limit the hero's ability to wear it.
     */
    public enum ArmorType {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    private ArmorType type;
    private HeroAttribute armorAttribute;

    /**
     * @param name Name of the piece of armor
     * @param requiredLevel The level required to wear the armor
     * @param slot The slot the armor goes into in the inventory
     * @param type The type of armor
     * @param armorAttribute The attributes (str, dex, int) of the piece of armor
     */
    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, HeroAttribute armorAttribute) {
        super(name, requiredLevel, slot);
        this.type = type;
        this.armorAttribute = armorAttribute;
    }

    // Getters
    public ArmorType getType() {
        return type;
    }

    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }
}
