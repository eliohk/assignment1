package no.accelerate.model.equipment;

/**
 * <h1>Weapon</h1>
 * Weapon is a subclass of the abstract class 'Item', which represents any equippable item
 * which has the Slot "Weapon".
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public class Weapon extends Item {
    /**
     * An enum to categorize the weapon into a specific
     * type which limit which hero can wield it.
     */
    public enum WeaponType {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    private double weaponDamage;
    private WeaponType type;

    /**
     * @param name The name of the weapon
     * @param requiredLevel The required level to wield the weapon
     * @param type The type that categorizes the weapon
     * @param weaponDamage how much damage the weapon inflicts
     */
    public Weapon(String name, int requiredLevel, WeaponType type, double weaponDamage) {
        super(name, requiredLevel, Slot.Weapon);
        this.type = type;
        this.weaponDamage = weaponDamage;
    }

    // Getters
    public WeaponType getType() {
        return type;
    }

    public double getWeaponDamage() {
        return weaponDamage;
    }
}
