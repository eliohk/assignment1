package no.accelerate.model.equipment;

/**
 * <h1>Item</h1>
 * An abstract class which contains shared fields for 'Item'-subclasses.
 *
 * @author  Khoi Le
 * @version 1.0
 * @since   2023-01-18
 */
public abstract class Item {
    /**
     * An enum to categorize the item into a slot
     */
    public enum Slot {
        Weapon,
        Head,
        Body,
        Legs
    }

    private String name;
    private int requiredLevel;
    private Slot slot;


    /**
     * @param name The name of the Item
     * @param requiredLevel The required level to use the item
     * @param slot The slot the item occupies when equipped/wielded.
     */
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    // Getters
    public Slot getSlot() {
        return slot;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return requiredLevel;
    }

}
