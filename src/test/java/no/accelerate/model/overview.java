package no.accelerate.model;

public class overview {
/*

    // REMEMBER: ARRANGE - ACT - ASSERT
    // Operation_condition_expectedoutcome

• When a Hero is created, it needs to have the correct name, level, and attributes. @@ DONE


• When a Heroes level is increased, it needs to increment by the correct amount and result in the correct
  attributes.
        o Creation and leveling tests need to be written for each sub class @@ DONE
        o A test to see if HeroAttribute is being added/increased correctly should also be written @@ DONE

@@ DONE
• When Weapon is created, it needs to have the correct name, required level, slot, weapon type, and damage

@@ DONE
• When Armor is created, it needs to have the correct name, required level, slot, armor type, and armor attributes

@@ DONE
• A Hero should be able to equip a Weapon, the appropriate exceptions should be thrown if invalid (level
  requirement and type)

@@ DONE

• A Hero should be able to equip Armor, the appropriate exceptions should be thrown if invalid (level requirement
  and type)

@@ DONE

• Total attributes should be calculated correctly
        o With no equipment
        o With one piece of armor
        o With two pieces of armor
        o With a replaced piece of armor (equip armor, then equip new armor in the same slot)

@@ DONE
• Hero damage should be calculated properly
        o No weapon equipped
        o Weapon equipped
        o Replaced weapon equipped (equip a weapon then equip a new weapon)
        o Weapon and armor equipped

@@ DONE

• Heroes should display their state correctly
*/
}
