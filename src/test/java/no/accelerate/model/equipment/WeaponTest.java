package no.accelerate.model.equipment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static no.accelerate.model.equipment.Item.*;
import static no.accelerate.model.equipment.Weapon.*;
import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    Weapon hammer;
    @BeforeEach
    void setUp() {
        hammer = new Weapon("Bronze Hammer", 1, WeaponType.Hammer, 2);
    }

    //• When Weapon is created, it needs to have the correct name, required level, slot, weapon type, and damage

    @Test
    void Weapon_ValidNameAfterCreation_ShouldPass() {
        // Arrange
        String expected = "Bronze Hammer";
        // Act
        String actual = hammer.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Weapon_ValidLevelAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = hammer.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Weapon_ValidSlotAfterCreation_ShouldPass() {
        // Arrange
        Slot expected = Slot.Weapon;
        // Act
        Slot actual = hammer.getSlot();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Weapon_ValidTypeAfterCreation_ShouldPass() {
        // Arrange
        WeaponType expected = WeaponType.Hammer;
        // Act
        WeaponType actual = hammer.getType();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Weapon_ValidDamageAfterCreation_ShouldPass() {
        // Arrange
        double expected = 2;
        // Act
        double actual = hammer.getWeaponDamage();
        // Assert
        assertEquals(expected, actual);
    }
}