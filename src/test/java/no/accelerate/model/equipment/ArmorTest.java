package no.accelerate.model.equipment;

import no.accelerate.model.hero.Hero;
import no.accelerate.model.hero.HeroAttribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static no.accelerate.model.equipment.Armor.*;
import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    Armor armor;

    @BeforeEach
    void setUp() {
        armor = new Armor("Dragon Platelegs", 1, Slot.Legs,
                ArmorType.Plate, new HeroAttribute(1, 1, 1));
    }

    // • When Armor is created, it needs to have the correct name, required level, slot, armor type, and armor attributes

    @Test
    void Armor_ValidNameAfterCreation_ShouldPass() {
        // Arrange
        String expected = "Dragon Platelegs";
        // Act
        String actual = armor.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Armor_ValidLevelAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = armor.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Armor_ValidSlotAfterCreation_ShouldPass() {
        // Arrange
        Slot expected = Slot.Legs;
        // Act
        Slot actual = armor.getSlot();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Armor_ValidTypeAfterCreation_ShouldPass() {
        // Arrange
        ArmorType expected = ArmorType.Plate;
        // Act
        ArmorType actual = armor.getType();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Armor_ValidAttributesAfterCreation_ShouldPass() {
        // Arrange
        int str = 1;
        int dex = 1;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = armor.getArmorAttribute();
        // Assert
        assertEquals(expected, actual);
    }

}