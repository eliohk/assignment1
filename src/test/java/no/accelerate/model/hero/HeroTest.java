package no.accelerate.model.hero;

import no.accelerate.model.equipment.Armor;
import no.accelerate.model.equipment.Armor.ArmorType;
import no.accelerate.model.equipment.Item.Slot;
import no.accelerate.model.equipment.Weapon;
import no.accelerate.model.equipment.Weapon.WeaponType;
import no.accelerate.model.exception.InvalidArmorException;
import no.accelerate.model.exception.InvalidWeaponException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    // Declare global variables
    Warrior warrior;
    Mage mage;
    Rogue rogue;
    Ranger ranger;

    Weapon axe;
    Weapon hammer;
    Weapon bigAxe;

    Armor leatherChaps;
    Armor leatherBody;
    Armor plateArmor;
    Armor clothArmor;
    Armor mailArmor;

    @BeforeEach
    void setUp() {
        // Initialize global variables
        warrior = new Warrior("Gimli");
        mage = new Mage("Gandalf");
        rogue = new Rogue("Legolas");
        ranger = new Ranger("Aragorn");

        axe = new Weapon("Common Axe", 1, WeaponType.Axe, 2);
        hammer = new Weapon("Common Hammer", 1, WeaponType.Hammer, 6);
        bigAxe = new Weapon("Rare Axe", 10, WeaponType.Axe, 20);
        plateArmor = new Armor("Common Plate Chest", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));
        leatherChaps = new Armor("Common Leather Chaps", 1, Slot.Legs, ArmorType.Leather, new HeroAttribute(0, 3, 0));
        leatherBody = new Armor("Common Leather Vest", 1, Slot.Body, ArmorType.Leather, new HeroAttribute(1, 5, 0));
        clothArmor = new Armor("Rare Tunic", 10, Slot.Body, ArmorType.Cloth, new HeroAttribute(0, 5, 10));
        mailArmor = new Armor("Chainmail Tunic", 1, Slot.Body, ArmorType.Mail, new HeroAttribute(3, 2, 1));
    }

    // Creation tests for hero subclass "Warrior"
    @Test
    void Warrior_ValidNameAfterCreation_ShouldPass() {
        // Arrange
        String expected = "Gimli";
        // Act
        String actual = warrior.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Warrior_ValidLevelAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = warrior.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Warrior_ValidAttributesAfterCreation_ShouldPass() {
        // Arrange
        int str = 5;
        int dex = 2;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = warrior.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void WarriorLevelUp_CorrectAttributeIncrementationOfLevel_ShouldPass() {
        // Arrange
        int str = 8;
        int dex = 4;
        int intel = 2;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        warrior.levelUp();
        HeroAttribute actual = warrior.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void WarriorLevelUp_ValidIncrementationOfLevel_ShouldPass() {
        // Arrange
        int expected = warrior.getLevel() + 1;
        // Act
        warrior.levelUp();
        int actual = warrior.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    // Creation tests for hero subclass "Mage"
    @Test
    void Mage_ValidNameAfterCreation_ShouldPass() {
        // Arrange
        String expected = "Gandalf";
        // Act
        String actual = mage.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Mage_ValidLevelAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = mage.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Mage_ValidAttributesAfterCreation_ShouldPass() {
        // Arrange
        int str = 1;
        int dex = 1;
        int intel = 8;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = mage.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void MageLevelUp_CorrectAttributeIncrementationOfLevel_ShouldPass() {
        // Arrange
        int str = 2;
        int dex = 2;
        int intel = 13;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        mage.levelUp();
        HeroAttribute actual = mage.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void MageLevelUp_ValidIncrementationOfLevel_ShouldPass() {
        // Arrange
        int expected = mage.getLevel() + 1;
        // Act
        mage.levelUp();
        int actual = mage.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    // Creation tests for hero subclass "Rogue"
    @Test
    void Rogue_ValidNameAfterCreation_ShouldPass() {
        // Arrange
        String expected = "Legolas";
        // Act
        String actual = rogue.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Rogue_ValidLevelAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = rogue.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Rogue_ValidAttributesAfterCreation_ShouldPass() {
        // Arrange
        int str = 2;
        int dex = 6;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = rogue.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void RogueLevelUp_CorrectAttributeIncrementationOfLevel_ShouldPass() {
        // Arrange
        int str = 3;
        int dex = 10;
        int intel = 2;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        rogue.levelUp();
        HeroAttribute actual = rogue.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void RogueLevelUp_ValidIncrementationOfLevel_ShouldPass() {
        // Arrange
        int expected = rogue.getLevel() + 1;
        // Act
        rogue.levelUp();
        int actual = rogue.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    // Creation tests for hero subclass "Ranger"
    @Test
    void Ranger_ValidNameAfterCreation_ShouldPass() {
        // Arrange
        String expected = "Aragorn";
        // Act
        String actual = ranger.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Ranger_ValidLevelAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = ranger.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Ranger_ValidAttributesAfterCreation_ShouldPass() {
        // Arrange
        int str = 1;
        int dex = 7;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = ranger.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void RangerLevelUp_CorrectAttributeIncrementationOfLevel_ShouldPass() {
        // Arrange
        int str = 2;
        int dex = 12;
        int intel = 2;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        ranger.levelUp();
        HeroAttribute actual = ranger.getLevelAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void RangerLevelUp_ValidIncrementationOfLevel_ShouldPass() {
        // Arrange
        int expected = ranger.getLevel() + 1;
        // Act
        ranger.levelUp();
        int actual = ranger.getLevel();
        // Assert
        assertEquals(expected, actual);
    }

    // • A Hero should be able to equip a Weapon, the appropriate exceptions should be thrown if invalid (level
    //  requirement and type)
    @Test
    void EquipWeapon_ValidValue_ShouldPass() throws InvalidWeaponException {
        // Arrange
        Weapon expected = axe;
        // Act
        warrior.equipWeapon(axe);
        Weapon actual = (Weapon) warrior.getInventory().get(Slot.Weapon);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void EquipWeapon_InvalidLevelValue_ShouldThrowInvalidWeaponExceptionWithAppropiateMessage() {
        // Arrange
        String heroName = "Gimli";
        String weaponName = "Rare Axe";
        int weaponLevel = 10;
        String expected = "[" + heroName + "] " + weaponName + " requires a level of " + weaponLevel + " to be equipped.";
        // Act
        Exception exception =
                assertThrows(InvalidWeaponException.class,
                        () -> warrior.equipWeapon(bigAxe));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void EquipWeapon_InvalidWeaponType_ShouldThrowInvalidWeaponExceptionWithAppropiateMessage() {
        // Arrange
        String heroName = "Gandalf";
        String weaponName = "Common Axe";
        String heroClass = "Mage";
        String expected = "[" + heroName + "] " + weaponName + " is not a weapon which the class " + heroClass + " can use!";
        // Act
        Exception exception =
                assertThrows(InvalidWeaponException.class,
                        () -> mage.equipWeapon(axe));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    // • A Hero should be able to equip Armor, the appropriate exceptions should be thrown if invalid (level requirement
    //  and type)

    @Test
    void EquipArmor_ValidValue_ShouldPass() throws InvalidArmorException {
        // Arrange
        Armor expected = plateArmor;
        // Act
        warrior.equipArmor(plateArmor);
        Armor actual = (Armor) warrior.getInventory().get(Slot.Body);
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void EquipArmor_InvalidLevelValue_ShouldThrowInvalidWeaponExceptionWithAppropiateMessage() {
        // Arrange
        String heroName = "Gandalf";
        String armorName = "Rare Tunic";
        int armorLevel = 10;
        String expected = "[" + heroName + "] " + armorName + " requires a level of " + armorLevel + " to be equipped.";
        // Act
        Exception exception =
                assertThrows(InvalidArmorException.class,
                        () -> mage.equipArmor(clothArmor));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void EquipArmor_InvalidArmorType_ShouldThrowInvalidWeaponExceptionWithAppropiateMessage() {
        // Arrange
        String heroName = "Gandalf";
        String armorName = "Common Plate Chest";
        String heroClass = "Mage";
        String expected = "[" + heroName + "] " + armorName + " is not an armor which the class " + heroClass + " can use!";
        // Act
        Exception exception =
                assertThrows(InvalidArmorException.class,
                        () -> mage.equipArmor(plateArmor));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    // • Total attributes should be calculated correctly
    //        o With no equipment
    //        o With one piece of armor
    //        o With two pieces of armor
    //        o With a replaced piece of armor (equip armor, then equip new armor in the same slot)
    @Test
    void TotalAttributes_ValidValueWithoutEquipment_ShouldPass() {
        // Arrange
        int str = 1;
        int dex = 7;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = ranger.totalAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void TotalAttributes_ValidValueWithOnePieceOfEquipment_ShouldPass() throws InvalidArmorException {
        // Arrange
        int str = 1;
        int dex = 10;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        ranger.equipArmor(leatherChaps);
        HeroAttribute actual = ranger.totalAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void TotalAttributes_ValidValueWithTwoPiecesOfEquipment_ShouldPass() throws InvalidArmorException {
        // Arrange
        int str = 2;
        int dex = 15;
        int intel = 1;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        ranger.equipArmor(leatherChaps);
        ranger.equipArmor(leatherBody);
        HeroAttribute actual = ranger.totalAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void TotalAttributes_ValidValueWithTwoPiecesOfEquipmentWhereOneIsReplacedByAnotherPiece_ShouldPass() throws InvalidArmorException {
        // Arrange
        int str = 4;
        int dex = 12;
        int intel = 2;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        ranger.equipArmor(leatherChaps);
        ranger.equipArmor(leatherBody);
        ranger.equipArmor(mailArmor);
        HeroAttribute actual = ranger.totalAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    // • Hero damage should be calculated properly
    //        o No weapon equipped
    //        o Weapon equipped
    //        o Replaced weapon equipped (equip a weapon then equip a new weapon)
    //        o Weapon and armor equipped
    @Test
    void Damage_ValidValueWithoutWeapon_ShouldPass() {
        // Arrange
        double weaponDamage = 1.0;
        double modifier = 5.0;
        double expected = weaponDamage * (1.0 + (modifier / 100.0));
        // Act
        double actual = warrior.damage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void Damage_ValidValueWithWeapon_ShouldPass() throws InvalidWeaponException {
        // Arrange
        double weaponDamage = 2;
        double modifier = 5;
        double expected = weaponDamage * (1.0 + (modifier / 100.0));
        // Act
        warrior.equipWeapon(axe);
        double actual = warrior.damage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void Damage_ValidValueWithWeaponWhichHasBeenReplacedByAnotherWeapon_ShouldPass() throws InvalidWeaponException {
        // Arrange
        double weaponDamage = 6;
        double modifier = 5;
        double expected = weaponDamage * (1.0 + (modifier / 100.0));
        // Act
        warrior.equipWeapon(axe);
        warrior.equipWeapon(hammer);
        double actual = warrior.damage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    void Damage_ValidValueWithWeaponAndArmor_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        // Arrange
        double weaponDamage = 2.0;
        double statModifier = 5.0;
        double armorModifier = 1.0;
        double expected = weaponDamage * (1.0 + ((statModifier + armorModifier) / 100.0));
        // Act
        warrior.equipWeapon(axe);
        warrior.equipArmor(plateArmor);
        double actual = warrior.damage();
        // Assert
        assertEquals(expected,actual);
    }

    // • Heroes should display their state correctly
    @Test
    void Display_ValidValueOfHeroAfterCreation_ShouldPass() {
        // Arrange
        String name = "Legolas";
        String heroClass = "Rogue";
        int level = 1;
        HeroAttribute totalAttributes = new HeroAttribute(2, 6, 1);
        double weaponDamage = 1.0;
        double modifier = 6.0;
        double damage = weaponDamage * (1.0 + modifier / 100.0);
        String expected = "--------------------------\n" +
                "Name: " + name +
                "\nClass: " + heroClass +
                "\nlevel: " + level +
                "\n\n > TotalAttributes \n" + totalAttributes +
                "\n\nDamage: " + damage + "\n";
        // Act
        String actual = rogue.display();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Display_ValidValueOfHeroAfterLevelUp_ShouldPass() {
        // Arrange
        String name = "Legolas";
        String heroClass = "Rogue";
        int level = 2;
        HeroAttribute totalAttributes = new HeroAttribute(3, 10, 2);
        double weaponDamage = 1.0;
        double modifier = 10.0;
        double damage = weaponDamage * (1.0 + modifier / 100.0);
        String expected = "--------------------------\n" +
                "Name: " + name +
                "\nClass: " + heroClass +
                "\nlevel: " + level +
                "\n\n > TotalAttributes \n" + totalAttributes +
                "\n\nDamage: " + damage + "\n";
        // Act
        rogue.levelUp();
        String actual = rogue.display();
        // Assert
        assertEquals(expected, actual);
    }
}