package no.accelerate.model.hero;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroAttributeTest {
    HeroAttribute attribute;

    @BeforeEach
    void setUp() {
        attribute = new HeroAttribute(1, 1, 1);
    }

    @Test
    void HeroAttribute_ValidStrengthValueAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = attribute.getStr();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void HeroAttribute_ValidDexterityValueAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = attribute.getDex();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void HeroAttribute_ValidIntelligenceValueAfterCreation_ShouldPass() {
        // Arrange
        int expected = 1;
        // Act
        int actual = attribute.getInt();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void Add_ValidValue_ShouldPass() {
        // Arrange
        int str = 2;
        int dex = 2;
        int intel = 2;
        HeroAttribute expected = new HeroAttribute(str, dex, intel);
        // Act
        HeroAttribute actual = attribute.add(attribute);
        // Assert
        assertEquals(expected, actual);
    }
}